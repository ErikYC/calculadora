import java.util.Scanner;

public class Calculadora{
		
	static float suma(int a,int b){
		return a+b;
	}
	static float resta(int a,int b){
		return a-b;
	}
	static float multi(int a,int b){
		return a*b;
	}
	static float division(int a,int b){
		return a/b;
	}
	public static void main(String[] args){
		Scanner capt = new Scanner(System.in);
		int a,b;
		System.out.println("Inserte los numeros a operar: ");
		a = capt.nextInt();
		b = capt.nextInt();
		System.out.println("Suma: " +  suma(a, b));
		System.out.println("Resta: " + resta(a, b));
		System.out.println("Multiplicacion: " + multi(a, b));
		System.out.println("Division: " + division(a, b));
	}
}
